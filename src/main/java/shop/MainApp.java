package shop;

public class MainApp {
    public static void main(String[] args) {
        // create shop controller and shop database connection
        ShopDatabase shopDatabase = new ShopDatabase();
        ShopController shopController = new ShopController(shopDatabase);

        boolean runShopMenu = true;

        while (runShopMenu) {
            // 1 - print shop menu
            runShopMenu = shopController.runShopMenu();
        }
    }
}
