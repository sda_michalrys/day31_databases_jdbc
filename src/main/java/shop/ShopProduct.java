package shop;

public class ShopProduct {
    private final int productId;
    private final String catalogNumber;
    private final String name;
    private final String description;

    public ShopProduct(int productId, String catalogNumber, String name, String description) {
        this.productId = productId;
        this.catalogNumber = catalogNumber;
        this.name = name;
        this.description = description;
    }

    public int getProductId() {
        return productId;
    }

    public String getCatalogNumber() {
        return catalogNumber;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
