package shop;

public enum ShopMenuOption {
//    PRINT_ALL_PRODUCTS("Print all products"),
//    PRINT_SINGLE_PRODUCTS("Print products info"),
//    ADD_PRODUCT("Add product"),
//    REMOVE_SINLGE_PRODUCT("Remove single product"),
//    UPDATE_PRODCUT("Update product"),
//    EXIT_FROM_MENU("Exit from shop menu");
    PA("Print all products"),
    P("Print products info"),
    A("Add product"),
    R("Remove single product"),
    U("Update product"),
    E("Exit from shop menu");

    String description;

    ShopMenuOption(String description) {
        this.description = description;
    }
}
