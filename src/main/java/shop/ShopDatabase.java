package shop;

import com.mysql.jdbc.Driver;

import java.sql.*;

public class ShopDatabase {
    static final String DB_URL = "jdbc:mysql://localhost/d31store";
    static final String USER = "mrys2";
    static final String PASSWORD = "ala123";
    private Connection connection;

    public void registerDriver() {
        // 1 - rejestrujemy drivera
        // register mysql driver
        try {
            Driver driver = new Driver();
            DriverManager.registerDriver(driver);
        } catch (SQLException e) {
            e.printStackTrace(); // tego nigdy tak się nie obsługuje -> na chwile obecną to zostawiamy
        }
    }

    public void establishConnection() {
        // 2 - trzeba nawiązać połączenie - potem trzeba zawsz zamykać !
        // establish connection
        try {
            connection = DriverManager.getConnection(DB_URL, USER, PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void closeConnection() {
        // 4 - close connection
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void printAllProducts() {
        // create statement
        Statement statement = null;
        try {
            statement = connection.createStatement();

            // here is question send to database
            ResultSet resultSet = null;
            try {
                connection.setAutoCommit(false);

                resultSet = statement.executeQuery("SELECT * FROM PRODUCTS");



                while (resultSet.next()) {
                    int product_id = resultSet.getInt("product_id");
                    String name = resultSet.getString("name");
                    String catalogNumber = resultSet.getString("catalog_number");
                    String description = resultSet.getString("description");

                    System.out.print("Product id: " + product_id);
                    System.out.print(" , name: " + name);
                    System.out.print(" , catalog number: " + catalogNumber);
                    System.out.println(" , description: " + description);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                if (resultSet != null) {
                    try {
                        resultSet.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
            // end question

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void printProductByProductId(int currentProductIdToShow) {
        // create statement
        Statement statement = null;
        try {
            statement = connection.createStatement();

            // here is question send to database
            ResultSet resultSet = null;
            try {
                resultSet = statement.executeQuery("SELECT *, name FROM PRODUCTS WHERE product_id =" + currentProductIdToShow);

                while (resultSet.next()) {
                    int product_id = resultSet.getInt("product_id");
                    String name = resultSet.getString("name");
                    String catalogNumber = resultSet.getString("catalog_number");

                    System.out.print("Product id: " + product_id);
                    System.out.print(" , name: " + name);
                    System.out.print(" , catalog number: " + catalogNumber);
                    System.out.println("");
                }

            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                if (resultSet != null) {
                    try {
                        resultSet.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
            // end question

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void updateProductById(int currentProductIdToUpdate, String newProductCatalogNumber, String newProductName, String newProductDescription) {
        // create statement
        Statement statement = null;
        try {
            statement = connection.createStatement();

            // here is question send to database
            try {
//                statement.executeUpdate("UPDATE products " +
//                        "SET catalog_number = '" + newProductCatalogNumber + "', " +
//                        "SET name ='" + newProductName + "', " +
//                        "SET description ='" + newProductDescription + "' " +
//                        "WHERE product_id =" + currentProductIdToUpdate);

                statement.executeUpdate("UPDATE products SET name ='" + newProductName
                        + "' WHERE product_id =" + currentProductIdToUpdate);
                statement.executeUpdate("UPDATE products SET catalog_number ='" + newProductCatalogNumber
                        + "' WHERE product_id =" + currentProductIdToUpdate);
                statement.executeUpdate("UPDATE products SET name ='" + newProductName
                        + "' WHERE product_id =" + currentProductIdToUpdate);
                statement.executeUpdate("UPDATE products SET name ='" + newProductName
                        + "' WHERE product_id =" + currentProductIdToUpdate);


            } catch (SQLException e) {
                e.printStackTrace();
            }
            // end question


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
