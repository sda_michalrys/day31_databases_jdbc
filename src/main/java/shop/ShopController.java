package shop;

import java.util.Scanner;

public class ShopController {
    private final ShopDatabase shopDatabase;
    private Scanner scanner = new Scanner(System.in);

    public ShopController(ShopDatabase shopDatabase) {
        this.shopDatabase = shopDatabase;
        shopDatabase.registerDriver();
    }


    public boolean runShopMenu() {
        System.out.println("-------------------------------------");
        System.out.println("Shop menu - choose options:");
        System.out.println("PA : " + ShopMenuOption.PA.description);
        System.out.println("P : " + ShopMenuOption.P.description);
        System.out.println("A : " + ShopMenuOption.A.description);
        System.out.println("R : " + ShopMenuOption.R.description);
        System.out.println("U : " + ShopMenuOption.U.description);
        System.out.println("E : " + ShopMenuOption.E.description);

        System.out.print(">> ");
        ShopMenuOption choosenOption = ShopMenuOption.valueOf(scanner.next().toUpperCase()); //todo lack of validation

        switch (choosenOption) {
            case PA:
                // 6 - print all
                printProductsAll();
                break;
            case P:
                // 2 - print product for given id
                System.out.println("\nGive product id = ");
                System.out.print(">>");
                // int currentProductIdToShow = scanner.nextInt();  //FIXME unhide
                int currentProductIdToShow = 2;
                printProduct(currentProductIdToShow);

                break;
            case A:
                // 5 - add product
//                add(new ShopProduct(1, "EAN 12354", "Coca-cola", "Good cola"));
//                add(new ShopProduct(2, "EAN 12355", "Pepsi cola", "Bad cola"));
//                add(new ShopProduct(3, "EAN 12356", "Fanta", "fake cola"));
//                add(new ShopProduct(4, "EAN 12357", "Sprite", "clean cola"));
                break;
            case R:
                // 3 - remove product for given id
//                int currentProcutIdToRemove = 4;
//                remove(currentProcutIdToRemove);
                break;
            case U:
                // 4 - update product for given id
                int currentProductIdToUpdate = 2;
                update(currentProductIdToUpdate, new ShopProduct(2, "EAN 12", "Tea", "test only"));
                break;
            case E:
                return false;
        }
        return true;
    }

    public void printProductsAll() {
        // shopDatabase.registerDriver();  moved to constructor
        shopDatabase.establishConnection();

        shopDatabase.printAllProducts();

        shopDatabase.closeConnection();
    }

    public void printProduct(int currentProductIdToShow) {
        shopDatabase.establishConnection();

        shopDatabase.printProductByProductId(currentProductIdToShow);

        shopDatabase.closeConnection();

    }

    public void add(ShopProduct shopProduct) {
    }

    public void remove(int productId) {
    }

    public void update(int currentProductIdToUpdate, ShopProduct shopProduct) {
        int pId = shopProduct.getProductId();
        String pCatNum = shopProduct.getCatalogNumber();
        String pName = shopProduct.getName();
        String pDes = shopProduct.getDescription();

        shopDatabase.establishConnection();

        shopDatabase.updateProductById(currentProductIdToUpdate, pCatNum, pName, pDes);

        shopDatabase.closeConnection();

    }
}
