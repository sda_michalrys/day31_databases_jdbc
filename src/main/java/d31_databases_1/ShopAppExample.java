package d31_databases_1;

import com.mysql.jdbc.Driver;

import java.sql.*;

public class ShopAppExample {
    static final String DB_URL = "jdbc:mysql://localhost/d31store";
    static final String USER = "mrys2";
    static final String PASSWORD = "ala123";
    private Connection connection;

    public static void main(String[] args) {

        // 1 - rejestrujemy drivera
        // register mysql driver
        try {
            Driver driver = new Driver();
            DriverManager.registerDriver(driver);
        } catch (SQLException e) {
            e.printStackTrace(); // tego nigdy tak się nie obsługuje -> na chwile obecną to zostawiamy
        }

        // 2 - trzeba nawiązać połączenie - potem trzeba zawsz zamykać !
        // establish connection
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(DB_URL, USER, PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
        }

//        // albo przez propertiesy
//        Properties properties = new Properties();
//        properties.setProperty("user", USER);
//        properties.setProperty("user", PASS);
//        DriverManager.getConnection(DB_URL, properties);

        // 3 - quest execution

        // execution 1
        Statement statement = null;
        PreparedStatement preparedStatement = null;
        try {
            //updateProducts(preparedStatement, connection);
            preparedStatement = connection.prepareStatement("UPDATE products SET description = ? WHERE product_id = ?");
            updateProducts(preparedStatement);

            statement = connection.createStatement();
            insertProduct(statement);
            findAllProducts(statement);  // <<<<<<<<<<<<<<<<<<<<<<< tutaj wyrzucone do metody aby bylo czytelniejsze

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if(preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        // 4 - close connection
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private static void updateProducts(PreparedStatement preparedStatement) {
        try {
            preparedStatement.setString(1, "bla bla bla");
            preparedStatement.setInt(2, 1);

            int updated = preparedStatement.executeUpdate();

            System.out.println(updated  + " updated products.");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private static void insertProduct(Statement statement) {
        try {
            int inserted = statement.
                    executeUpdate("INSERT INTO products VALUES" + "(2,'EAN 12355', 'Pepsi cola2', 'My fav drink')");

            System.out.println(inserted + " new products added.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void findAllProducts(Statement statement) {
        ResultSet resultSet = null;
        try {
            resultSet = statement.executeQuery("SELECT product_id, name FROM PRODUCTS");

            while (resultSet.next()) {
                int product_id = resultSet.getInt("product_id");
                String name = resultSet.getString("name");

                System.out.print("Product id: " + product_id);
                System.out.println(" and name: " + name + ".");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
